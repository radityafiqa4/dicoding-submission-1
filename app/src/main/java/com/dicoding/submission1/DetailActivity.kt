package com.dicoding.submission1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dicoding.submission1.Model.Git
import kotlinx.android.synthetic.main.activity_detail.*

class DetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_USER = "EXTRA_USER"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        supportActionBar?.let{
            it.title = "Detail Github"
            it.setDisplayHomeAsUpEnabled(true)
        }

        val user = intent.getParcelableExtra(EXTRA_USER) as Git
        username.text = user.username
        nama.text = user.name
        company.text = user.company
        location.text = user.location
        followers.text = user.follower
        following.text = user.follower
        repository.text = user.repository
        Glide.with(this)
            .load(user.avatar)
            .apply(RequestOptions())
            .into(avatars)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


}