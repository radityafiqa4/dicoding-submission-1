package com.dicoding.submission1.Adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.dicoding.submission1.DetailActivity
import com.dicoding.submission1.Model.Git
import com.dicoding.submission1.R

class GithubAdapter(val listGit: ArrayList<Git>) : RecyclerView.Adapter<GithubAdapter.ListViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ListViewHolder {
        val view: View = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.card_list, viewGroup, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int = listGit.size

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        val user = listGit[position]

        Glide.with(holder.itemView.context)
            .load(user.avatar)
            .apply(RequestOptions())
            .into(holder.avatar)
        holder.name.text = user.name
        holder.location.text = user.location

        val mContext = holder.itemView.context

        holder.itemView.setOnClickListener {
            val mGitUser = Intent(mContext, DetailActivity::class.java)
            mGitUser.putExtra(DetailActivity.EXTRA_USER, user)
            mContext.startActivity(mGitUser)
        }
    }

    inner class ListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val name: TextView = itemView.findViewById(R.id.name)
        val location: TextView = itemView.findViewById(R.id.locations)
        val avatar: ImageView = itemView.findViewById(R.id.avatar)
    }
}