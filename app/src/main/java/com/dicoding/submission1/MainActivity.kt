package com.dicoding.submission1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dicoding.submission1.Adapter.GithubAdapter
import com.dicoding.submission1.Common.GitData
import com.dicoding.submission1.Model.Git
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.card_list.view.*


class MainActivity : AppCompatActivity() {


    private lateinit var rvGithub: RecyclerView
    private var list: ArrayList<Git> = arrayListOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val actionBar = supportActionBar
        actionBar?.title = "Github Users"

        rvGithub = findViewById(R.id.GithubList)
        rvGithub.setHasFixedSize(true)

        list.addAll(GitData.listData)
        showRecyclerList()
    }

    private fun showRecyclerList() {
        rvGithub.layoutManager = LinearLayoutManager(this)
        val listCorpAdapter = GithubAdapter(list)
        rvGithub.adapter = listCorpAdapter
    }


}


